const Sequelize = require('sequelize');

const directorModel = require('./models/director');
const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const actorModel = require('./models/actor');
const movieActorModel = require('./models/movieActor');
const memberModel = require('./models/member');
const bookingModel = require('./models/booking');
const copieModel = require('./models/copie');
/*
    1 Nombre de la base de datos
    2 Usuario
    3 Contraseña
    4 Objeto de configuraciòn  ORM
*/

const sequelize = new Sequelize('video-club',
'root','1230', {
    host: '127.0.0.1',
    dialect: 'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Copie = copieModel(sequelize, Sequelize);

//Un genero puede tener muchas peliculas
Genre.hasMany(Movie, {as:'movies'});
//Una pelicula tiene un genero
Movie.belongsTo(Genre, {as:'genre'});

//Un director puede tener muchas peliculas
Director.hasMany(Movie, {as:'movies'});
//Una pelicula tiene un director
Movie.belongsTo(Director, {as:'director'});

//Un actor participa en muchas peliculas
MovieActor.belongsTo(Movie, {foreignKey:'movieId'});

//En una pelicula participan muchos actores
MovieActor.belongsTo(Actor, {foreignKey:'actorId'});

Movie.belongsToMany(Actor, {
    foreignKey: 'actorId',
    as:'actors',
    through: 'movies_actors'
});

Actor.belongsToMany(Movie, {
    foreignKey: 'movieId',
    as:'movies',
    through: 'movies_actors'
});

Booking.belongsTo(Copie, {foreignKey:'copieId'});

Booking.belongsTo(Member, {foreignKey:'memberId'});

Copie.belongsToMany(Member, {
    foreignKey: 'memberId',
    as:'members',
    through: 'bookings'
});

Member.belongsToMany(Copie, {
    foreignKey: 'copieId',
    as:'copies',
    through: 'bookings'
});

Movie.hasMany(Copie, {as:'copies'});

Copie.belongsTo(Movie, {as:'movie'});

sequelize.sync({
    force:true
}).then(()=>{
    console.log('Base de datos sincronizada.')
});

module.exports = {Director, Genre, Movie };