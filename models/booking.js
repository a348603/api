module.exports = (sequelize, type) => {
    const Booking = sequelize.define('bookings', {
    id: {type: type.INTEGER, primaryKey:true, autoIncrement:true},
    date: type.DATE(2),
    memberId: type.INTEGER,
    copyId: type.INTEGER
    });
    return Booking;
}